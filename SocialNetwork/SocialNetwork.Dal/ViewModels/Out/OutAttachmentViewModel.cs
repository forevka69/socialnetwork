﻿namespace SocialNetwork.Dal.ViewModels.Out
{
    public class OutAttachmentViewModel
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public string Preview { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string DisplayName { get; set; }
    }
}

