export interface IAttachment {
    id: number;
    path: string;
    preview: string;
    width: number;
    height: number;
    displayName: string;
}